const path = require("path");
const nodeExternals = require("webpack-node-externals");

module.exports = {
    entry: "./src/server.js",
    target: "node",

    output: {
        filename: "server.js",
        path: path.resolve(__dirname, "dist"),
        publicPath: "",
    },

    module: {
        rules: [
            {
                test: /.(js)$/,
                include: [path.resolve(__dirname, "src"), path.resolve(__dirname, "client/src")],
                loader: "babel-loader",

                options: {
                    plugins: ["syntax-dynamic-import", "@babel/plugin-proposal-class-properties"],

                    presets: [
                        [
                            "@babel/preset-env",
                            {
                                modules: false,
                            },
                        ],
                        [
                            "@babel/preset-react",
                            {
                                modules: false,
                            },
                        ],
                    ],
                },
            },
            {
                test: /\.html$/,
                loader: "html-loader",
            },
        ],
    },
    node: {
        __dirname: true,
        __filename: false,
    },
    externals: [nodeExternals()],

    devServer: {
        open: true,
    },
};
