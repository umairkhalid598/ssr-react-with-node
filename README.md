# Get Mobile

**Requirments:**<br />

Node version >= 10.16.3 <br />
Npm version >= 6.9.0

**Setup:** <br />

Create a .env file and write
`Node_EVN=develoment` and `secret=<any string>`. The secret key is used to create access token.
<br />
```
npm run setup
```

or

```
yarn setup
```

**Start**
```
npm run webpack
```

or

```
yarn webpack
```

