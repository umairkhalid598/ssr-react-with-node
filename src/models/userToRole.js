module.exports = (sequelize, DataTypes) => sequelize.define("UserToRole", {
    userId: {
        type: DataTypes.INTEGER,
        field: "user_id",
    },
    roleId: {
        type: DataTypes.INTEGER,
        field: "role_id",
    },
}, {
    tableName: "user_to_roles",
    timestamps: false,
});
