const admin = require("./admin");
const role = require("./role");
const user = require("./user");

module.exports = (app) => {
    admin(app);
    role(app);
    user(app);
};
