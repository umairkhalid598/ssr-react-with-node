const adminController = require("../controllers").admin;
const authorize = require("../helper/authorize");
const validate = require("../controllers/admin/validation");
const { ROLE } = require("../constants/index");

module.exports = (app) => {
    app.get("/api/admins", authorize([ROLE.Admin]), adminController.get);
    app.post("/api/admins", authorize([ROLE.Admin]), validate("create"), adminController.create);
    app.delete("/api/admins/:id", authorize([ROLE.Admin]), adminController.delete);
    app.post("/api/admins/login", adminController.login);
};
