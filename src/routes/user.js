const userContoller = require("../controllers").user;
const authorize = require("../helper/authorize");
const validate = require("../controllers/user/validation");
const { ROLE } = require("../constants/index");

module.exports = (app) => {
    app.get("/api/users", authorize([ROLE.Admin, ROLE.User]), userContoller.get);
    app.post("/api/users", validate("create"), userContoller.create);
    app.get("/api/users/:id", authorize([ROLE.Admin, ROLE.User]), userContoller.getById);
    app.delete("/api/users/:id", authorize([ROLE.Admin, ROLE.User]), userContoller.delete);
    app.post("/api/users/login", validate("create"), userContoller.login);
};
