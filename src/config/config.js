// eslint-disable-next-line consistent-return
module.exports = () => {
    if (process.env.NODE_ENV === "development") {
        return {
            secret: process.env.SECRET,
        };
    }
    if (process.env.NODE_ENV === "test") {
        return {
            secret: process.env.SECRET,
        };
    }
    if (process.env.NODE_ENV === "production") {
        return {
            secret: process.env.SECRET,
        };
    }
};
