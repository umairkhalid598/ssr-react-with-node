import '@babel/polyfill'
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const express = require('express');
const fs = require('fs');
const morgan = require('morgan');
const path = require('path');
const React = require('react');
const ReactDOMServer = require('react-dom/server');
const { StaticRouter } = require("react-router");
const swaggerUi = require('swagger-ui-express');
const winston = require('winston');
const dotEnv = require('dotenv');
import App from "../client/src/app"
const pageNotFound = require('./helper/notFound');
const swaggerDocument = require('../swagger.json');
const expressValidator = require('express-validator');
const errorHandler = require("./helper/errorHandler");
const result = dotEnv.config();

if (result.error) {
    throw result.error
}

const app = express();
const port = process.env.PORT || 3000;

const logger = winston.createLogger({
    level: 'debug',
    format: winston.format.json(),
    defaultMeta: { service: 'user-service' },
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({ filename: 'stacktrace.log' }),
    ],
});

logger.add(new winston.transports.Console({
    format: winston.format.simple(),
}));

app.use(morgan('dev'));

// Parsing incoming requests.
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

const serverRenderer = async (req, res, next) => {
    if (req.url.startsWith('/api')) {
        app.use(expressValidator());

        await require('./routes')(app);
        app.use(pageNotFound);
        app.use(function (err, req, res, next) {
            errorHandler(err, req, res, next);
        });

        next();
    } else {
        const context = {};

        const clientApp = ReactDOMServer.renderToString(
            <StaticRouter location={req.url} context={context}>
                <App/>
            </StaticRouter>
        );

        const indexFile = path.resolve('./dist/public/index.html');

        fs.readFile(indexFile, 'utf8', (err, data) => {
            if (err) {
                console.error('Something went wrong:', err);
                return res.status(500)
                    .send('Oops, better luck next time!');
            }

            return res.send(
                data.replace('<div id="root"></div>', `<div id="root">${clientApp}</div>`)
            );
        });
    }
};

app.use(express.static('dist/public'));

app.use( serverRenderer);


app.listen(port, () => {
    console.log('Server is running on port 3000')
});
