const { validationResult } = require("express-validator/check");
const roleService = require("../../service/role");
const errorHandler = require("../../helper/errorHandler");

module.exports = {
    get: async (req, res) => {
        try {
            const roles = await roleService.get();
            res.json({ status: res.statusCode, data: roles.map(role => role.asJsonMap()) });
        } catch (e) {
            errorHandler(e, req, res);
        }
    },
    create: async (req, res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                res.status(422).json({ status: 422, errors: errors.array() });
                return;
            }
            const role = await roleService.create(req.body);

            res.json({ status: res.statusCode, data: role.asJsonMap() });
        } catch (e) {
            errorHandler(e, req, res);
        }
    },
};
