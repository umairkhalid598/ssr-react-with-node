const { validationResult } = require("express-validator/check");
const { ROLE } = require("../../constants/index");
const adminService = require("../../service/admin");
const errorHandler = require("../../helper/errorHandler");

module.exports = {
    get: async (req, res) => {
        try {
            const admins = await adminService.get(ROLE.Admin);
            res.json({ status: res.statusCode, data: admins.map(admin => admin.asJsonMap()) });
        } catch (e) {
            errorHandler(e, req, res);
        }
    },
    login: async (req, res) => {
        try {
            const login = await adminService.login(req.body);
            res.status(201).send(login);
        } catch (e) {
            errorHandler(e, req, res);
        }
    },
    create: async (req, res) => {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                res.status(422).json({ status: 422, errors: errors.array() });
                return;
            }
            const admin = await adminService.create(req.body, ROLE.Admin);
            res.json({ status: res.statusCode, data: admin.asJsonMap() });
        } catch (e) {
            errorHandler(e, req, res);
        }
    },
    delete: async (req, res) => {
        try {
            await adminService.delete(Number(req.params.id));
            res.json({ status: res.statusCode, msg: "Admin deleted successfully" });
        } catch (e) {
            errorHandler(e, req, res);
        }
    },
};
