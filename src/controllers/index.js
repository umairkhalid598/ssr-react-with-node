const admin = require("./admin");
const role = require("./role");
const user = require("./user");

module.exports = {
    admin,
    role,
    user,
};
