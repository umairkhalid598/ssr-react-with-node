const jwt = require("jsonwebtoken");
const { ROLE, USER_STATE } = require("../constants/index");
const { User, Role } = require("../models");
const { secret } = require("../config/config")();

module.exports = {
    get: async role => User.findAll({
        include: [{
            model: Role,
            as: "roles",
            where: { name: role },
        }],
    }),
    login: async (adminUserObj) => {
        const {
            email, password,
        } = adminUserObj;

        const user = await User.findOne({
            where: {
                email,
            },
            include: [{
                model: Role,
                as: "roles",
                where: { name: ROLE.Admin },
            }],
        });
        if (!user) {
            const error = new Error("Email does not exist");
            error.status = 401;
            throw error;
        }
        if (user.validatePassword(password)) {
            const {
                id, firstName, lastName, fullName,
            } = user;
            const token = jwt.sign(
                { sub: user.id, email, roles: user.roles.map(role => role.name) },
                secret,
            );
            return {
                id, email, firstName, lastName, fullName, token,
            };
        }
        const error = new Error("Invalid email or password");
        error.status = 401;
        throw error;
    },

    create: async (adminUserObj, role) => {
        const {
            firstName, lastName, email, password,
        } = adminUserObj;
        const roleObj = await Role.findOne({ where: { name: role } });
        let user = await User.findOne({ where: { email } });
        if (!roleObj) {
            const error = new Error("Role does not exist");
            error.status = 422;
            throw error;
        }
        if (!user) {
            user = await User.create({
                firstName,
                lastName,
                email,
                password,
                state: USER_STATE.Registered,
            });
        }
        await user.addRoles(roleObj);
        return User.findOne({ where: { email }, include: "roles" });
    },
    delete: async id => User.destroy({ where: { id } }),
};
