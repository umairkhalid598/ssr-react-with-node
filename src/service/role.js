const { Role } = require("../models");

module.exports = {
    get: async () => Role.findAll(),
    create: async (params) => {
        const {
            name,
            description,
        } = params;
        return Role.create({
            name,
            description,
        });
    },
};
